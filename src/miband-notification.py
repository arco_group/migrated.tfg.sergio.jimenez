import logging
import struct
import queue
from types import SimpleNamespace as SimpleObj
from functools import lru_cache
from hexdump import hexdump
from Crypto.Cipher import AES
from gattlib import GATTRequester, DiscoveryService
import sys
import argparse


class MiBand4Requester(GATTRequester):
    """Library to use the GATT Protocol for Bluetooth LE devices.
    """
    def __init__(self, log, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.log = log
        self.handle_queues = {}

    def on_notification(self, handle, data):
        dump = hexdump(data, result="return").replace("\n", "\n  ")
        handle_name = Handles.get_name(handle)
        self.log.debug(f" + notification on handle: 0x{handle:04x}:{handle_name}")
        self.log.debug(f" + message dump: \n  {dump}")

        # fist 3B in data: 1B + handle number, strip them off
        self._get_queue(handle).put(data[3:])

    def wait_for_notification(self, handle, timeout=10):
        q = self._get_queue(handle)

        try:
            return q.get(timeout=timeout)
        except queue.Empty:
            return None

    def _get_queue(self, handle):
        q = self.handle_queues.get(handle)
        if q is None:
            q = queue.Queue()
            self.handle_queues[handle] = q
        return q


class MiBand4:
    """Main object of the library

     Arguments:
        mac: MAC address in a string way.
        name: particular name of the device.
        token: security token from MiFit app.
        autoconnect:it will connect upon object creation.
    """
    def __init__(self, name, mac, token=None, autoconnect=False):
        self.given_name = name
        self.mac = mac
        self.token = token
        self.requester = None

        self.log = logging.getLogger(name)
        self.log.setLevel(logging.INFO)

        if autoconnect:
            self.connect()

    def _enable_notifications(self, handle, enabled):
        self.log.debug(" {} notifications on handle 0x{:04x}:{}...".format(
            "enabling" if enabled else "disabling",
            handle, Handles.get_name(handle)
        ))
        cmd = b"\x01\x00" if enabled else b"\x00\x00"
        self.requester.write_by_handle(handle, cmd)

    def _authorize(self):
        # 1. request a random number from the device
        self._enable_notifications(Handles.AUTH_NOTIF, True)
        self.requester.write_cmd(Handles.AUTH, b"\x02\x00")

        # 2. wait notification with number from device
        while True:
            data = self.requester.wait_for_notification(Handles.AUTH)
            if data is None:
                self.log.error(" - could not get authorization!")
                return False

            if data[:3] == b"\x10\x02\x01":
                data = data[3:]
                break

        # 3. encode number and send it back
        aes = AES.new(self.token, AES.MODE_ECB)
        msg = b'\x03\x00' + aes.encrypt(data)
        self.requester.write_cmd(Handles.AUTH, msg)

        # 4. wait for message indicating a valid auth
        data = self.requester.wait_for_notification(Handles.AUTH)
        self._enable_notifications(Handles.AUTH_NOTIF, False)

        if data == b"\x10\x03\x01":
            self.log.info(" - authorization OK")
            return True

        if data is None:
            self.log.error(" - could not get authorization!")
        else:
            sel.log.error(" - invalid token for authorization!")
        return False

    @classmethod
    def discover(cls, timeout=10, devname="Mi Smart Band 4"):
        service = DiscoveryService("hci0")
        devices = service.discover(timeout)

        retval = []
        for address, name in devices.items():
            if name != devname:
                continue
            retval.append(address)
        return retval

    def connect(self):
        """Used to create a connection with the Mi Band. It may take some
        time, depending on many factors as LE params, channel usage,
        etc. 
        """
        
        self.log.info(f" connecting to '{self.mac}'...")
        self.requester = MiBand4Requester(self.log, self.mac, False)
        self.requester.connect(True, security_level="medium")
        self.log.info(" OK, connected")

        if self.token is None:
            return False

        self.log.info("[INFO] token provided, try to get authorization...")
        return self._authorize()

    def disconnect(self):
        """Used to close the connection with the Mi Band when not needed anymore.
        """
        self.requester.disconnect()

    def is_connected(self):
        """Check if Mi Band is currently connected. Returns a bool value.
        """
        return self.requester.is_connected()

    @lru_cache()
    def get_name(self):
        """Retrieves (and stores) the internal device name. Returns a string.
        """
        data = self.requester.read_by_uuid(Chars.DEVICE_NAME)
        return data[0].decode()

    def set_alert_level(self, level):
        """Set the level of alerts. `level` may be one of `silent`, `mild` or `high`.
        """

        # https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/
        # Gatt/Xml/Characteristics/org.bluetooth.characteristic.alert_level.xml

        code = {
            'silent': '\x00',
            'mild':   '\x01',
            'high':   '\x02',
        }

        self.log.warning("Mi Band 4 may not support this operation (yet).")
        self.requester.write_cmd(Handles.ALERT_LEVEL, code[level])
        self.log.info(f" alert level set to {level}")

    def send_alert(self, message, type="email", count=1):
        """Sends a notification alert to the device.
        `type` could be: email, call, missed-call, sms.
        `count` is the number of alerts in the server (number of messages, etc.)
        """

        # https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/
        # Gatt/Xml/Characteristics/org.bluetooth.characteristic.new_alert.xml

        code = {
            "email":       1,
            "call":        3,
            "missed-call": 4,
            "sms":         5,
        }

        cmd = bytes([code[type], count])
        if "\n" in message:
            cmd += "\x0a\x0a\x0a"
        cmd += message.encode()

        self.requester.write_by_handle(Handles.NEW_ALERT, cmd)
        self.log.info(" notification sent!")


class Consts:
    """Constants used in this library.
    """

    MSG_AUTH_RANDOM_NUMBER     = b"\x10\x02\x01"

class Chars:
    """Mi Band characteristic UUID constants for notification."""

    # these UUIDs are defined by the Bluetooth SGI
    DEVICE_NAME         = "00002a00-0000-1000-8000-00805f9b34fb"
    ALERT_LEVEL         = "00002a06-0000-1000-8000-00805f9b34fb"
    ALERT_CTRL          = "00002a44-0000-1000-8000-00805f9b34fb"
    NEW_ALERT           = "00002a46-0000-1000-8000-00805f9b34fb"

    # these UUDs are custom, defined by Xiaomi (extracted by RE)
    AUTH                = "00000009-0000-3512-2118-0009af100700"


class Handles:
    """Mi Band characteristic handle constants."""

    NEW_ALERT                = 0x001c
    ALERT_LEVEL              = 0x0023
    AUTH                     = 0x0060
    AUTH_NOTIF               = 0x0061

    @classmethod
    def get_name(cls, handle):
        for k, v in cls.__dict__.items():
            if v == handle:
                return k
        return f"<unknown: {handle}>"

def _getOptions(args=sys.argv[1:]):

    """Define different options to manage MiBand4.

    Returns:
        options: Returns the values that have been chosen.
    """

    parser = argparse.ArgumentParser(description="Xiaomi Mi Band notifications.")
    parser.add_argument("-o", "--options", required=True, choices=["email", "call", "sms"], help="Options to send a notification: email, call, sms")
    parser.add_argument("-m", "--message",  type=str, default="", help="Message to send.")
    parser.add_argument("-i", "--iteration", type=int, default=1, help="Iterations to send a notification.")

    options = parser.parse_args(args)
    return options

def _load_devices():
    """Loads the devices that are defined in the file `device.csv`.
    """
    with open('devices.csv') as fd:
        for line in fd.readlines():
            line = line.strip()
            if not line or line.startswith("#"):
                continue
            try:
                miband_id, mac, token = map(str.strip, line.split("|"))
                token = bytes.fromhex(token)
                
            except ValueError:
                raise Exception(f"[ERROR] invalid format in line: {line}")

        fd.close()
    return miband_id, mac, token


if __name__ == "__main__":
    
    option = _getOptions()

    try:
        miband_id, mac, token = _load_devices()
        message = option.message
        
        miband = MiBand4(str(miband_id), mac, token)
        while True:
            miband.connect()
            if miband.is_connected():
                break

        miband.send_alert(message, type=option.options, count=option.iteration)
        miband.disconnect()
    
    except Exception as error:
        print(error)




