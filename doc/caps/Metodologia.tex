\chapter{Metodología}
\label{cap:Metodologia}

\drop{E}n este capítulo se describirán y se explicarán cada una de las metodologías seguidas durante el desarrollo del proyecto, así como la planificación de las diferentes tareas que se han realizado junto con una breve justificación de su elección.

\section{Desarrollo incremental e iterativo}

Este \acs{tfg} se ha basado en el proceso de desarrollo iterativo e incremental, abstrayendo el proyecto en varios módulos. Para ello, el trabajo se ha dividido en diferentes bloques denominados \emph{hitos} o \emph{iteraciones}. En cada una de estas iteraciones, se ha empleado un flujo de trabajo o \emph{workflow} en el que debe incluirse una serie de tests o pruebas junto con su implementación correspondiente. 

El desarrollo incremental es una estrategia organizativa donde diferentes partes de un sistema se diseñan y se desarrollan en diferentes momentos. Con esta estrategia, cada uno de los hitos del proyecto han sido desarrollados de forma individual, es decir, el desarrollo de las tareas no interfería con otras. Estas iteraciones permiten estructurar de forma sencilla cada una de las funcionalidades del sistema final que hemos definido anteriormente. A medida que las iteraciones se completaban, el proyecto final iba evolucionando hasta convertirse en un sistema más completo. 

A la hora de gestionar el proceso evolutivo, se han priorizado varios hitos en función del grado de dificultad que suponía su desarrollo. Al mismo tiempo, estos hitos también han sido priorizados en base a las funcionalidades necesarias consiguiendo un mayor progreso en el proyecto.

Gracias a la metodología de desarrollo iterativo e incremental, cada uno de los hitos han sido desarrollados y abstraídos individualmente sin que pueda interferir con otros. Este proyecto ha seguido esta estrategia con el fin de estructurar cada una de las funcionalidades que conforman el sistema. En resumen, esta metodología de desarrollo nos permite conocer los resultados sin tener acabado el proyecto final ya que en todo momento hemos podido visualizar cómo avanza la aplicación, tanto en la gestión como en la monitorización de los datos. 

\section{Desarrollo Dirigido por Tests (TDD)}

\emph{\acs{tdd}} o \emph{“Test-Driven Development”} consiste en una metodología de desarrollo incluida en la metodología \emph{XP} \cite{jurado}. Esta metodología permite la implementación de diversas funcionalidades empleando las líneas de código imprescindibles con el fin de evitar redundancias y conseguir un software modular, reutilizable y abierto a cambios. El algoritmo sólo tiene tres pasos:  

\begin{enumerate}
    \item Escribir la especificación de un requisito que esté detallado en forma de test.
    
    \item Implementar el código funcional para que el test pase de forma correcta.
    
    \item Refactorizar el código para eliminar duplicidad, así como la posibilidad de añadir mejoras y nuevas funcionalidades en un futuro. 
    
\end{enumerate}

Esta metodología no solo permite crear una batería de pruebas que proporcionen una buena cobertura en nuestro sistema, sino que también dirigirán el desarrollo de cada una de las funcionalidades obteniendo un producto de alta calidad. A partir de la definición de cada uno de los tests, nuestra responsabilidad es implementar el código necesario para obtener la funcionalidad deseada. Además, esta metodología de desarrollo permite minimizar la cantidad de código fuente escrito eliminando las partes redundantes con el propósito de obtener un código optimizado y facilitar el mantenimiento de este.  

Esta técnica está unificada con la metodología de desarrollo incremental e iterativo, ya que en cada iteración tenemos la posibilidad de añadir distintas funcionalidades que se enmarcan en una serie de tests, los cuales deben pasar de forma satisfactoria. Al mismo tiempo, a la hora de implementar una nueva funcionalidad, debemos asegurarnos de que las funcionalidades implementadas anteriormente siguen funcionando correctamente junto con la nueva.

\section{Test de Usabilidad}

Aparte de establecer las distintas metodologías de desarrollo, es importante recalcar la importancia que tienen los tests de usabilidad de cara a la implementación de esta plataforma, en especial al servicio web de Phyx.io. Estos tests consisten en una serie de pruebas o acciones que se ejecutan con el objetivo de evaluar la facilidad de uso de un sistema web desde la perspectiva del usuario final. 

Con la finalidad de ofrecer la mejor experiencia a cualquier usuario, los tests de usabilidad se deben cumplir en todos los procesos de desarrollo de la plataforma web. Las pruebas deben realizarse:

\begin{itemize}
    \item Antes de empezar con la etapa de diseño.
    
    \item Con el objetivo de completar y añadir más información en medio del proceso de desarrollo.
    
    \item Si existen opiniones que refutan el diseño.
    
    \item Una vez desplegado el sistema.
\end{itemize}


En resumen, los tests de usabilidad deben realizarse en las etapas de lluvia de ideas o \emph{brainstorming}, durante la creación de diversos componentes y en la evaluación tras su lanzamiento. 

\subsection{ICF-US}

Como hemos mencionado anteriormente, el nivel de usabilidad dependerá de los usuarios que prueben las distintas funcionalidades que proporciona el sistema de forma exitosa. En nuestro caso, Phyx.io contiene una plataforma web que está compuesta por numerosos tipos de usuarios y por ello tendremos que adaptar dicha plataforma a los usuarios para que puedan usar cualquier funcionalidad de forma sencilla. 

Para la evaluación de estos tests de usabilidad se elaboró un documento donde el observador presenta una escala de usabilidad basándose en la \ac{icf}, desarrollada por la Organización Mundial de la Salud en 2001 \cite{CIF}. Esta escala sitúa al usuario como el centro de la evaluación con el objetivo de hacer hincapié en el desarrollo de una plataforma web que sea sencilla de emplear. La escala basada en el \acs{icf} está compuesta a su vez de dos subescalas:

\begin{enumerate}
    \item \textit{ICF-US I}. Define una escala general de evaluación de la usabilidad. Este método permite evaluar un prototipo funcional. Si el grado de usabilidad es menor a 10 puntos, se deberá aplicar el formato \emph{ICF-US II}.
    
    \item \textit{ICF-US II}. Consiste en un cuestionario con una evaluación más detallada donde se clasificarán los componentes de un prototipo en barreras o facilitadores. Este método permite evaluar una maqueta en una fase temprana de su diseño permitiendo modificar dichas barreras en facilitadores. 
\end{enumerate}


\subsection{ICF-US I}

La \emph{ICF-US I} \cite{ICF-US} es un método de evaluación de la usabilidad que permite al evaluador comprobar si el sistema o el producto tiene los requisitos mínimos de usabilidad. Este método se evalúa en una escala de 6 componentes como evaluadores. Como podemos observar en la Figura~\ref{fig:ICF-USI_table}, todos los ítems se puntuarán con valores que oscilarán entre el valor más negativo (-3) hasta el más positivo (3). En el caso de que un participante no responda a un ítem específico, entonces se indicará como no aplicable (N/A) y se le asignará el valor medio de los ítems restantes redondeando a la unidad. Una vez finalizada la evaluación, se procede a la suma de todos los valores; si el valor es superior a 10 puntos se considera que el sistema tiene una buena usabilidad. No obstante, si el valor es menor que 10 puntos, significa que el prototipo debe ser mejorado y deberá evaluarse con el método \emph{ICF-US II}. En el Anexo~\ref{cap:AnexoB} se encuentra un ejemplo del ICF-US I.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\linewidth]{figs/ICF-USI_table.png}
	\caption[Escala de valoración de elementos individuales basada en los calificadores de la ICF]{Escala de valoración de elementos individuales basada en los calificadores de la ICF \cite{ICF-US}}
	\label{fig:ICF-USI_table}
\end{figure}


\subsection{ICF-US II}
La escala \emph{ICF-US II} \cite{ICF-US} contiene una serie de ítems que permiten identificar cada uno de los componentes que se implementarán para la solución final. Como hemos mencionado anteriormente, estos componentes se clasifican en barreras o facilitadores. En el caso de que el usuario defina el componente como una barrera, éste debe identificar las características que hace que el componente sea una barrera, como se indica en la Figura~\ref{fig:ICF-USII_example}. Con esta evaluación, es posible identificar los componentes que deben modificarse para la mejora del prototipo en fase de desarrollo. El ICF-US II se divide en tres partes:

\begin{enumerate}
    \item \textit{Componentes de la aplicación}. Esta parte debe desarrollarse para cada solución digital teniendo en cuenta los diferentes componentes. 
    
    \item \textit{Usabilidad detallada}. Esta parte debe desarrollarse teniendo en cuenta las diferentes funcionalidades de interacción, así como la interfaz del usuario.
    
    \item \textit{Evaluación global del sistema}. Esta parte incluye una pregunta general sobre la toma de contacto con el sistema.
    
\end{enumerate}

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\linewidth]{figs/ICF-USII_example.png}
	\caption[Ejemplo de ICF-US II]{Ejemplo de ICF-US II \cite{ICF-US}}
	\label{fig:ICF-USII_example}
\end{figure}

Todas estas partes serán evaluadas de la misma manera que en la escala ICF-US I, donde el máximo valor será 3 y el mínimo será -3. Todas estas partes se pueden visualizar en el Anexo~\ref{cap:AnexoC}.

\section{Scrum}
Después de describir las metodologías de desarrollo y también en qué consisten los tests de usabilidad, es necesario destacar que en este proyecto se han empleado metodologías ágiles con la finalidad de obtener el mejor resultado posible en el producto final. \emph{Scrum} consiste en una metodología de gestión de proyectos que aplica una serie de buenas prácticas cuyo objetivo es reducir la complejidad del desarrollo del proyecto final. 

El desarrollo del proyecto se divide en Sprints. Un Sprint es un conjunto de tareas o iteraciones que serán proporcionadas como \emph{entregas parciales} dependiendo del orden de prioridad de ejecución en un proyecto Scrum. Esta metodología de gestión de proyectos es necesaria en el desarrollo de proyectos complejos donde los requisitos pueden variar de un momento a otro o que sea necesario obtener resultados en un breve periodo de tiempo.

A lo largo del desarrollo de este proyecto, se han llevado a cabo diversas iteraciones donde el tiempo de duración ha sido de 2 semanas para establecer una revisión general y revisar cada uno de los hitos realizados. Para la planificación de cada uno de estos hitos, hemos empleado la herramienta \emph{MeisterTask}\footnote{\url{https://www.meistertask.com}} donde dividimos el panel en varios artefactos relacionados en el ámbito de Scrum:
\begin{itemize}
    \item \textit{Product Backlog}. Es una lista de cada una de las funcionalidades ajustadas a una prioridad de ejecución que contendrá una serie de descripciones indicando las pautas que se tienen que seguir para desarrollar el producto.
    
    \item \textit{ToDo}. Esta lista contiene cada una de las tareas que se deben realizar a lo largo del desarrollo del proyecto final.
    
    \item \textit{In Progress}. Esta sección indica todas las tareas que se están realizando para el próximo Sprint. En cada una de las tareas, se establecerán varias anotaciones con el objetivo de registrar los métodos que se han empleado a lo largo de su implementación y desarrollo.
    
    \item \textit{Done}. Esta sección indica cada una de las tareas convertidas en entregables o incrementos del producto de cada Sprint. En nuestro caso, los Sprints tienen una duración de unas 2 semanas, estableciendo una reunión indicando los entregables aceptados y las retrospectivas.
    
\end{itemize}

Esta metodología de gestión de proyectos nos permite establecer una serie de iteraciones que estarán complementadas con la metodología de desarrollo iterativo e incremental, donde a medida que las iteraciones llegan a completarse, el proyecto final irá evolucionando hasta convertirse en un sistema más completo. Al mismo tiempo, hemos empleado esta metodología ya que nos permite abstraer el proyecto en varios hitos que serán entregados en cada Sprint, permitiendo integrar los entregables que fueron aceptados anteriormente sin que interfieran unos con otros.

\section{Planificación}

\begin{table}
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
\rowcolor[HTML]{EFEFEF} 
TAREAS                                                                                    & ENERO                    & FEBRERO                  & MARZO                    & ABRIL                    & MAYO                     & JUNIO                    \\ \hline
\cellcolor[HTML]{EFEFEF}Proyecto preliminar                                               & \cellcolor[HTML]{34CDF9} &                          &                          &                          &                          &                          \\ \hline
\cellcolor[HTML]{EFEFEF}Estudios teóricos                                                 &                          & \cellcolor[HTML]{34CDF9} &                          &                          &                          &                          \\ \hline
\cellcolor[HTML]{EFEFEF}\begin{tabular}[c]{@{}l@{}}Implementación/\\ Testing\end{tabular} &                          & \cellcolor[HTML]{34CDF9} & \cellcolor[HTML]{34CDF9} & \cellcolor[HTML]{34CDF9} & \cellcolor[HTML]{34CDF9} & \cellcolor[HTML]{34CDF9} \\ \hline
\cellcolor[HTML]{EFEFEF}Integración                                                       &                          &                          &                          &                          & \cellcolor[HTML]{34CDF9} & \cellcolor[HTML]{34CDF9} \\ \hline
\cellcolor[HTML]{EFEFEF}Documentación                                                     &                          & \cellcolor[HTML]{34CDF9} & \cellcolor[HTML]{34CDF9} & \cellcolor[HTML]{34CDF9} & \cellcolor[HTML]{34CDF9} & \cellcolor[HTML]{34CDF9} \\ \hline
\end{tabular}
\caption[Etapas del proyecto]{Etapas del proyecto}
\label{tab:tasks}
\end{table}

En este apartado, se mostrarán cada una de las tareas que se han llevado a cabo a la hora de realizar el proyecto final. Además, se dará una breve explicación acerca de los hitos más importantes que conforman el diseño del sistema. En la Tabla~\ref{tab:tasks} se pueden ver todas las tareas que han sido desarrolladas, así como el despliegue del proyecto final. 

\subsection{Proyecto preliminar}

A lo largo de esta fase, surgieron las primeras ideas del proyecto final, definiendo la metodología y los diferentes hitos. Al principio, la idea era desarrollar una plataforma cuyo objetivo era la gestión de los datos recogidos por los diferentes \emph{totems} situados en cada una de las instalaciones. No obstante, surgió la idea de poder integrar y monitorizar los parámetros importantes obtenidos de la pulsera comercial \emph{Mi Band 4}. Para concluir con esta fase, se dio por finalizado y establecido el apartado del anteproyecto. 

\subsection{Estudios teóricos}

En esta fase del proyecto, se realizó un estudio de todas las herramientas y metodologías necesarias para el desarrollo del sistema. La interfaz de gestión de datos se ha implementado con \emph{Django} (\emph{framework} de desarrollo web basado en \emph{Python}) que utiliza, en este caso, una base de datos \emph{SQLite3}. En cambio, a la hora de monitorizar los parámetros recogidos por la pulsera Mi Band 4, hemos optado por herramientas de monitorización como \emph{Grafana} así como una base de datos \ac{orm}, \emph{PostgreSQL}. A la hora de poder monitorizar los parámetros de la pulsera Mi Band 4, hemos acudido a profesionales de la medicina para aprender a analizar de manera precisa los datos obtenidos por la pulsera y a sacar conclusiones a partir de los datos recogidos.

\subsection{Implementación e integración}

En esta sección, definiremos cada una de las tareas generales llevadas a cabo mediante una estrategia de \ac{tdd}, así como la integración del servicio de monitorización de la pulsera Mi Band 4. En la Tabla~\ref{tab:implementation} podemos observar cada una de las tareas que componen la implementación y la integración de la plataforma de Phyx.io.

\begin{table}
\begin{tabular}{|
>{\columncolor[HTML]{EFEFEF}}l |
>{\columncolor[HTML]{FFFFFF}}l |
>{\columncolor[HTML]{FFFFFF}}l |
>{\columncolor[HTML]{FFFFFF}}l |
>{\columncolor[HTML]{FFFFFF}}l |
>{\columncolor[HTML]{FFFFFF}}l |
>{\columncolor[HTML]{FFFFFF}}l |}
\hline
TAREAS                           & \cellcolor[HTML]{EFEFEF}ENERO & \cellcolor[HTML]{EFEFEF}FEBRERO & \cellcolor[HTML]{EFEFEF}MARZO & \cellcolor[HTML]{EFEFEF}ABRIL & \cellcolor[HTML]{EFEFEF}MAYO & \cellcolor[HTML]{EFEFEF}JUNIO \\ \hline
Módulo de usuarios               &                               & \cellcolor[HTML]{34CDF9}        & \cellcolor[HTML]{34CDF9}      &                               &                              &                               \\ \hline
Módulo de instalaciones          &                               & \cellcolor[HTML]{34CDF9}        & \cellcolor[HTML]{34CDF9}      & \cellcolor[HTML]{34CDF9}      &                              &                               \\ \hline
Módulo de totems                 &                               &                                 &                               & \cellcolor[HTML]{34CDF9}      &                              &                               \\ \hline
Módulo de ejercicios y rutinas   &                               &                                 &                               & \cellcolor[HTML]{34CDF9}      & \cellcolor[HTML]{34CDF9}     & \cellcolor[HTML]{34CDF9}      \\ \hline
Integración de la monitorización &                               &                                 &                               &                               & \cellcolor[HTML]{34CDF9}     &                               \\ \hline
Servicio de notificación         &                               &                                 &                               &                               & \cellcolor[HTML]{34CDF9}     & \cellcolor[HTML]{34CDF9}      \\ \hline
Despliegue de Phyx.io            &                               &                                 &                               &                               &                              & \cellcolor[HTML]{34CDF9}      \\ \hline
\end{tabular}
\caption[Etapas de la implementación e integración]{Etapas de la implementación e integración}
\label{tab:implementation}
\end{table}

\subsubsection{Módulo de usuarios}

A la hora de implementar este módulo, necesitábamos conocer cada uno de los tipos de usuario que van a acceder a la plataforma, así como los diferentes permisos de cada una de las funcionalidades que posee el sistema. Durante el mes de febrero, se realizó la implementación de los perfiles como: entrenadores, mánagers, administrador y usuarios finales. Durante el mes de marzo, se implementaron los diferentes \emph{dashboards} de los usuarios que están asociados a una instalación o residencia, es decir los mánagers y entrenadores.

\subsubsection{Módulo de instalaciones y totems}

La gestión de información en una instalación (o residencia) es de suma importancia debido a que está relacionada con el módulo de usuarios. Después de varios días de asignación de roles, se definieron cada uno de los permisos asociados a los distintos usuarios para visualizar y editar las características de cualquier instalación. Al mismo tiempo, el módulo de gestión de totems ha sido implementado durante el mes de abril siendo el módulo de menor duración. 

\subsubsection{Módulo de ejercicios y rutinas}

Esta etapa tuvo que ser implementada en los meses posteriores ya que es dependiente de los módulos mencionados anteriormente. Para la implementación de este módulo nos hemos reunido con los usuarios finales con el fin de gestionar los datos de cada ejercicio o rutina relacionada a un usuario. Además, en esta reunión hemos acordado la elección de los parámetros más importantes para el tratamiento y monitorización de la salud de los usuarios de edad adulta a través de la pulsera Mi Band 4.

\subsubsection{Integración y monitorización}

Durante el mes de mayo, se diseñó la integración del servicio de monitorización de la pulsera Mi Band 4, así como la implementación del módulo de visualización y almacenamiento de los parámetros asociados a los usuarios de edad adulta. Durante el mes de mayo, se identificó también el método de despliegue más conveniente para la integración del servicio de registro de datos (miband-dc) de la pulsera Mi Band 4 junto con la plataforma web de \emph{Phyx.io} permitiendo así la ejecución de ambos servicios de forma independiente. Al mismo tiempo, se realizó la integración de ambos servicios.

\subsubsection{Servicio de notificación}

Tanto en el mes de mayo como en el mes de junio, la idea consistió en desarrollar un servicio capaz de mandar una serie de notificaciones a través de la pulsera Mi Band 4 a los usuarios y que sea independiente tanto al servicio de monitorización como al servicio de gestión de datos.

\subsubsection{Despligue de Phyx.io}

En el último mes de junio, el sistema web de Phyx.io ha sido desplegado en un servidor web al que todos los usuarios puedan acceder desde cualquier dispositivo que esté conectado a Internet. 

\subsection{Documentación}

Esta última tarea se ha realizado a lo largo de todo el proceso de desarrollo del proyecto. Esta fase consiste en la elaboración y diseño de este informe. También fue necesario seleccionar todos los documentos informativos en las fases iniciales del proyecto, para la búsqueda y análisis de nuevos materiales y herramientas para el desarrollo de este proyecto.

Al mismo tiempo, hemos documentado y realizado una serie de tests de usabilidad con el objetivo de obtener orientación de cara a mejorar dicha usabilidad, gracias a la colaboración de los usuarios finales. Estos tests son muy importantes ya que nos permiten evaluar los componentes necesarios para satisfacer las necesidades de los usuarios.

\section{Herramientas}

En este apartado se presentarán todas las herramientas, tanto hardware como software, que han sido necesarias para el desarrollo completo del proyecto junto con una breve descripción de sus características y su propósito en el proyecto. En la Figura~\ref{fig:tools} se muestran todas las herramientas utilizadas.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\linewidth]{figs/Herramientas TFG.jpg}
	\caption[Herramientas de desarrollo]{Herramientas de desarrollo}
	\label{fig:tools}
\end{figure}

\subsection{Herramientas Hardware}
Las herramientas hardware usadas a lo largo del proyecto han sido las siguientes: 
\begin{enumerate}
    \item \textit{Ordenador de Sobremesa}: Ordenador con un Procesador Intel Core i5, HDD de 500 GB y 8 GB de RAM.
    
    \item \textit{Ordenador portátil}: Portátil ASUS, Procesador Intel Core i7-6500U, HDD de 1TB y 12 GB de RAM. 
    
    \item \textit{Pulsera Mi Band 4}\footnote{\url{https://www.mi.com/es/mi-smart-band-4/} (visitado 12-02-2021)}: Dispositivo de Xiaomi para la monitorización de la salud y bienestar del usuario.
    
    \item \textit{Raspberry Pi 4}: Quad-Core Cortex-A72 64 bits a 1.5GHz de velocidad, 4GB de RAM LPDDR4. 
\end{enumerate}

\subsection{Herramientas Software}

\subsubsection{Sistema Operativo y Lenguajes de Programación}

Como sistema operativo se ha hecho uso de \emph{Ubuntu 20.04}, un sistema operativo con núcleo Linux formado principalmente por software libre, y de \emph{Raspbian}. Esta última es una distribución del sistema operativo GNU/Linux basado en \emph{Debian} para la SBC Raspberry Pi. Como lenguaje de programación se ha empleado \emph{Python} en su versión 3.8 como lenguaje principal. Para la programación y diseño de la plataforma web de \emph{Phyx.io}, hemos empleado el framework web basado en Phyton, \emph{Django}.

\subsubsection{Aplicaciones}
\begin{itemize}
    \item \textit{Visual Studio Code}: Editor de código compatible con una gran cantidad de lenguajes de programación para construir y depurar cualquier aplicación o sistema. 

    \item \textit{\LaTeX}: Sistema de composición de texto, que permite la creación de documentos científicos de forma sencilla y con una alta calidad tipográfica gracias a sus plantillas predeterminadas o creadas. 

    \item \textit{Overleaf}: Editor web cuyo propósito es escribir documentos de texto haciendo uso del sistema \LaTeX.   

    \item \textit{MeisterTask}: Herramienta online de gestión de proyectos que permite planificar de forma eficaz la creación de nuevos productos, su lanzamiento, gestión de proyectos, etc. 

    \item \textit{Git}: Sistema de control de versiones que permite planificar proyectos y colaborar en el desarrollo de código. 

    \item \textit{Gattlib}\footnote{ \url{https://pypi.org/project/gattlib/} (visitado 17-05-2021)}: Librería desarrollada utilizando el protocolo \emph{GATT} para los dispositivos Bluetooth LE. 

    \item \textit{PostgreSQL}: Sistema de gestión de bases de datos relacional orientado a objetos y de código abierto. 

    \item \textit{Grafana}: Software libre basado en licencia de Apache 2.0. Permite la visualización y el formato de parámetros métricos, así como la creación de cuadros de mando y gráficos a partir de múltiples fuentes.
    
    \item \textit{Nginx}: Es un servidor web que también puede ser usado como proxy inverso, balanceador de carga y proxy para protocolos de correo.
    
    \item \textit{Shark Hosting}\footnote{\url{https://sharkhosting.co.uk/} (visitado 22-06-2021)}: Shark Hosting es un proveedor de alojamiento para páginas web que ofrece un gran ancho de banda y espacio de disco ilimitados dependiendo de la cuota pagada. Son ideales para desplegar VPS (Virtual Private Server).
\end{itemize}