# PHYX.IO

This repository contains Sergio Jiménez's final degree project. The main goal is to develop an application to support physical activity through monitoring, supervision and personalised intervention.

Este repositorio contiene el Trabajo Fin de Grado de Sergio Jiménez del Coso. El objetivo principal es desarrollar una aplicación de soporte a la actividad física mediante la monitorización, supervisión e intervención personalizada.